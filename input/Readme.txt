In order to download the read data selected in the MPS application you have to do the following:

1) Unarchive the zip file in the local directory that you want to save your data.

2) Run the run_wget.sh command file that will download the data from the Nanuq application.

3) The script will prompt you for a login and password. Please use your Nanuq login and password.
   Once credentials are validated, the download will start.
   
4) For each file downloaded, a md5 file was also downloaded, to validate the downloaded files run this following command:
   md5sum -c *.md5.
   
   Please contact the Nanuq Team in case of any problem occuring regarding the download process.
Nanuq Team