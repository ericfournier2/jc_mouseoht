mkdir output

$MUGQIC_PIPELINES_HOME/pipelines/chipseq/chipseq.py -s '1-15' \
    -l debug \
    -r input/readset.txt \
    -d input/design.txt \
    -o $SCRATCH/Mouse.2OHT \
    --config $MUGQIC_PIPELINES_HOME/pipelines/chipseq/chipseq.base.ini \
        $MUGQIC_PIPELINES_HOME/pipelines/chipseq/chipseq.guillimin.ini \
        $MUGQIC_PIPELINES_HOME/resources/genomes/config/Mus_musculus.GRCm38.ini

