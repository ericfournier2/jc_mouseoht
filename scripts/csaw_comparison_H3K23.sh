#!/bin/bash
#PBS -l nodes=1:ppn=8
#PBS -l walltime=24:00:00
#PBS -A fhh-112-aa
#PBS -o csaw_H3K23.stdout
#PBS -e csaw_H3K23.stderr
#PBS -V
#PBS -N csaw_H3K23

cd /sb/project/fhh-112-aa/Mouse.OHT

module load  mugqic/R_Bioconductor/3.2.3_3.2
Rscript csaw_for_pipeline.R input/csaw_H3K23_design.txt WT-H3K23 mm10 csaw_H3K23