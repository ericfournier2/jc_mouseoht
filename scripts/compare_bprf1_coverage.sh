cd /gs/scratch/efournier/Mouse.2OHT
for bam in alignment/2OHT-*/2OHT-*.sorted.dup.bam
do
    bn=`basename $bam .sorted.dup.bam`
    for j in `seq -10 10`
    do
        let factor=10000*j
        let start=113316498-factor
        let end=113317743-factor
        nAlign=`samtools view $bam 6:$start-$end | cut -f 10 | wc -l`
        echo $j:$bn:$nAlign
    done
done
