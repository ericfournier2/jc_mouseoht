library(eric.utils)

ann.object = select.annotations("mm10")

results.regions = list()
results.annotations = list()
summary.list = list()
for(mark in c("H3K14", "H3K23")) {
    input.file = paste0("output/csaw_", mark, "/csaw_clusters")
    input.data = read.table(input.file, sep="\t", header=TRUE)
    
    # Fix chromosome names
    input.data$seqnames = paste0("chr", input.data$seqnames)
    input.data$seqnames = gsub("chrMT", "chrM", input.data$seqnames)

    # Keep only significant regions
    input.data = input.data[input.data$FDR <= 0.05 & abs(input.data$best.logFC) > 1, ]
    
    # Annotate the regions.
    input.regions = GRanges(input.data)
    annotated.regions = annotate.region(input.regions, ann.object)
    
    # Write annotated regions to a file.
    write.table(as.data.frame(annotated.regions), file=paste0(input.file, ".annotated.txt"), sep="\t", col.names=TRUE, row.names=FALSE)
    sink(paste0(input.file, ".summary.txt"))
    print(annotated.regions)
    
    results.regions[[mark]] = input.regions
    results.annotations[[mark]] = annotated.regions
    
    cat("\nTotal significant regions: ", length(input.regions), "\n")
    cat("Up: ", sum(input.regions$best.logFC > 0), "\n")
    cat("Down: ", sum(input.regions$best.logFC < 0), "\n")
    sink(NULL)
}

intersect.object = build.intersect(GRangesList(results.regions))
middle.intersection = intersect.overlap(intersect.object)

middle.annotation = annotate.region(middle.intersection, ann.object)

# Map back to FC
for(mark in c("H3K14", "H3K23")) {
    indices = findOverlaps(middle.intersection, results.regions[[mark]], select="first")
    
    mcols(middle.intersection)[[paste0(mark, ".FDR")]] = results.regions[[mark]][indices]$FDR
    mcols(middle.intersection)[[paste0(mark, ".FC")]] = results.regions[[mark]][indices]$best.logFC
}

write.table(as.data.frame(middle.intersection), file="output/csaw_intersection.txt", sep="\t", col.names=TRUE, row.names=FALSE)

# Cross-tabs.
up.down = data.frame(H3K14=factor(ifelse(middle.intersection$H3K14.FC > 0, "UP", "DOWN")), 
                     H3K23=factor(ifelse(middle.intersection$H3K23.FC > 0, "UP", "DOWN")))
table(up.down)
