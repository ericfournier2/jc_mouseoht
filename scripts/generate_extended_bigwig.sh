cd /gs/scratch/efournier/Mouse.2OHT

# Generate a version of the fai file with chr prefix
sed -e 's/^\([1-9MXY]\)/chr\1/' /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.GRCm38/genome/Mus_musculus.GRCm38.fa.fai > $SCRATCH/mm10.ucsc.fai

for sample in alignment/*
do
    sampleName=`basename $sample`    

    cat > $sampleName.sh <<EOF
#!/bin/bash
#PBS -l nodes=1:ppn=8
#PBS -l walltime=8:00:00
#PBS -A fhh-112-aa
#PBS -o $sampleName.sh.stdout
#PBS -e $sampleName.sh.stderr
#PBS -V
#PBS -N $sampleName.sh

module load mugqic/samtools/0.1.19 mugqic/bedtools/2.22.1 mugqic/ucsc/20140212 mugqic/python/2.7.8

cd /gs/scratch/efournier/Mouse.2OHT

mkdir -p tracks/$sampleName tracks/bigWig

# Calculate normalization factor
nmblines=\$(samtools view -F 256 alignment/$sampleName/$sampleName.sorted.dup.bam | wc -l)
scalefactor=0\$(echo "scale=2; 1 / (\$nmblines / 10000000);" | bc)

#genomeCoverageBed -bg -split -scale \$scalefactor -ibam alignment/$sampleName/$sampleName.sorted.dup.bam -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.GRCm38/genome/Mus_musculus.GRCm38.fa.fai > tracks/$sampleName/$sampleName.bedGraph
/home/efournier/.local/bin/bamCoverage --normalizeUsingRPKM --extendReads 200 --binSize 10 \
                                       --bam alignment/$sampleName/$sampleName.sorted.dup.bam \
                                       --outFileFormat bedgraph  --outFileName tracks/$sampleName/extended.bedgraph

sed -e 's/^\([1-9MXY]\)/chr\1/' tracks/$sampleName/extended.bedgraph |
sort -k1,1 -k2,2n > tracks/$sampleName/extended.ucsc.bedgraph
rm tracks/$sampleName/extended.bedgraph

bedGraphToBigWig tracks/$sampleName/extended.ucsc.bedgraph $SCRATCH/mm10.ucsc.fai  tracks/bigWig/$sampleName.ucsc.extended.bw
rm tracks/$sampleName/extended.ucsc.bedgraph
EOF

    qsub $sampleName.sh    
done

#nmblines=$(samtools view -F 256 alignment/$sampleName/$sampleName.sorted.dup.bam | wc -l)
#scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc)
#genomeCoverageBed -bg -split -scale $scalefactor -ibam alignment/$sampleName/$sampleName.sorted.dup.bam -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.GRCm38/genome/Mus_musculus.GRCm38.fa.fai > tracks/$sampleName/$sampleName.bedGraph

# bedGraphToBigWig alignment/$sampleName/$sampleName.sorted.dup.bedgraph /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.GRCm38/genome/Mus_musculus.GRCm38.fa.fai  tracks/bigWig/$sampleName.bw