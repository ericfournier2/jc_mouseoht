#!/bin/bash

for i in TSS TES Heatmap
do
    cat > $i.sh << EOF
module load mugqic/R_Bioconductor/3.2.3_3.2
Rscript scripts/GenerateMetagenes.R $i
EOF

    qsub -m ae -M $JOB_MAIL -W umask=0002 -A "fhh-112-aa" \
         -d /sb/project/fhh-112-aa/Mouse.OHT  \
         -o $i.stdout -e $i.stderr -N $i.metagene \
         -l walltime=2:00:0 -q metaq -l nodes=1:ppn=8 $i.sh
done
